var mymap = L.map('leafletmap').setView([43.2133, 2.32325000000003], 13);

L.tileLayer('https://api.tiles.mapbox.com/v4/{id}/{z}/{x}/{y}.png?access_token=pk.eyJ1IjoidGhvbWFzY2FkZWkiLCJhIjoiY2pxeHQzaWcxMDI1YjQycHBhYmVrYTAwbiJ9.BvaqbQlCb3sJgL_IusmUGg', {
	attribution: 'Map data &copy; <a href="https://www.openstreetmap.org/">OpenStreetMap</a> contributors, <a href="https://creativecommons.org/licenses/by-sa/2.0/">CC-BY-SA</a>, Imagery © <a href="https://www.mapbox.com/">Mapbox</a>',
	maxZoom: 18,
	id: 'mapbox.streets',
	accessToken: 'your.mapbox.access.token'
}).addTo(mymap);

// Icônes personnalisées
const theIcon = L.icon({
	iconUrl: 'pics/simplon.png',
	iconSize: [50, 50],
	iconAnchor: [20, 40],
});
var marker = L.marker([43.2133, 2.32325000000003], {icon: theIcon}).addTo(mymap);php