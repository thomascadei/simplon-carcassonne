<?php

namespace App\Controller;

use App\Entity\Candidate;
use App\Form\CandidacyType;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;

class CandidacyController extends AbstractController
{
    /**
     * @Route("/candidacy", name="candidacy")
     */
    public function index(Request $request)
    {
        $candidate = new Candidate;
        $form = $this->createForm(CandidacyType::class, $candidate);
        $form->handleRequest($request);

        if ($form->isSubmitted() && $form->isValid()) {

        }
        return $this->render('candidacy/index.html.twig', [
            'controller_name' => 'CandidacyController',
            'form' => $form->createView(),
        ]);
    }
}
