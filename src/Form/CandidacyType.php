<?php

namespace App\Form;

use App\Entity\Candidate;
use Doctrine\DBAL\Types\DateTimeType;
use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\Extension\Core\Type\BirthdayType;
use Symfony\Component\Form\Extension\Core\Type\ChoiceType;
use Symfony\Component\Form\Extension\Core\Type\EmailType;
use Symfony\Component\Form\Extension\Core\Type\NumberType;
use Symfony\Component\Form\Extension\Core\Type\SubmitType;
use Symfony\Component\Form\Extension\Core\Type\TelType;
use Symfony\Component\Form\Extension\Core\Type\TextType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolver;

class CandidacyType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('genus', ChoiceType::class, [
                'choices' => [
                    'M.' => 'male',
                    'Mme.' => 'female'
                ],
                'label' => 'Civilité',
            ])
            ->add('name', TextType::class, [
                'label' => 'Prénom'
            ])
            ->add('surname', TextType::class, [
                'label' => 'Nom'
            ])
            ->add('date_of_birth', BirthdayType::class, [
                'label' => 'Date de naissance (jour-mois-année)',
                'widget' => 'choice',
                'format' => 'dd-MM-yyyy',
            ])
            ->add('address', TextType::class, [
                'label' => 'Adresse'
            ])
            ->add('post_code', NumberType::class, [
                'label' => 'Code Postal'
            ])
            ->add('city', TextType::class, [
                'label' => 'Ville'
            ])
            ->add('phone_number', TelType::class, [
                'label' => 'Téléphone'
            ])
            ->add('mail_address', EmailType::class, [
                'label' => 'E-mail'
            ])
            ->add('skills', ChoiceType::class, [
                'label' => 'Comment estimes-tu ton niveau en développement web ?',
                'choices' => [
                    'Débutant' => 'beginner',
                    'Basique' => 'elementary',
                    'Moyen' => 'intermediate',
                    'Bon' => 'advanced',
                    'Expert' => 'expert',
                ],
            ])
            ->add('school_level', ChoiceType::class, [
                'label' => 'Quel est le niveau du dernier diplôme que tu aies obtenu ?',
                'choices' => [
                    'Je n\'ai pas de diplôme' => '0',
                    'CAP, BEP, BEPC' => '3',
                    'Baccalauréat' => '4',
                    'DEUG, BTS, DUT, DEUST' => '5',
                    'Licence, Master, Doctorat' => '6',
                ],
            ])
            ->add('situation', ChoiceType::class, [
                'label' => 'Quelle est ta situation professionnelle actuelle ?',
                'choices' => [
                    'Indépendant' => 'independant',
                    'Salarié' => 'employee',
                    'Étudiant' => 'student',
                    'Sans emploi' => 'unemployed',
                ],
            ])
            ->add('submit', SubmitType::class, [
                'attr' => ['class' => 'btn btn-block bg-red'],
                'label' => 'Je candidate !'
            ]);
    }

    public function configureOptions(OptionsResolver $resolver)
    {
        $resolver->setDefaults([
            'data_class' => Candidate::class,
        ]);
    }
}
